//
//  NSLayoutConstraint+voidxin.h
//  AhaPhotoLab
//
//  Created by zhangxin on 2019/11/27.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSLayoutConstraint (voidxin)
@property(nonatomic, assign) IBInspectable BOOL adapterScreen;
@end

NS_ASSUME_NONNULL_END
